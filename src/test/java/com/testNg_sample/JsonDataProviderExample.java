package com.testNg_sample;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonFactory;

/*import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;*/


import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class JsonDataProviderExample {
	@Test(dataProvider = "dp")
	public void JasonDataProviderExample(Map<String, List<JSONPojo>> map) {
		System.out.println(map);
		List<JSONPojo> pojoList= new ArrayList<JSONPojo>();
		for (Map.Entry<String, List<JSONPojo>> entry : map.entrySet()) {
			//System.out.println("[Key] : " + entry.getKey() + " [Value] : " + entry.getValue());
			pojoList = entry.getValue();
            System.out.println("*** JSON File Contents ***");
			for(JSONPojo pojo:pojoList) {

	            System.out.println("Id : "+pojo.getId()+" First Name : "+pojo.getFirstName()+" Last Name : "+pojo.getLastName()+" Gender : "+pojo.getGender()+" IP Address : "+pojo.getIp_address());
			}
			
		}
	}

	@DataProvider(name="dp")
	public Object[][] dp() throws JsonParseException, JsonParseException, IOException {
		Map<String, JSONPojo> jsonMap = null;
		ObjectMapper mapper = new ObjectMapper();

		mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

		try {
			
			 jsonMap = mapper.readValue(new File(
					 "MOCK_DATA.json"),
	                    new TypeReference<Map<String, List<JSONPojo>>>(){});


		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new Object[][] { { jsonMap } };
	}

}
