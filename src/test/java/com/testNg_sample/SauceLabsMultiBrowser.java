package com.testNg_sample;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.rmi.UnexpectedException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.Test;

public class SauceLabsMultiBrowser extends TestBase {
	@Test(dataProvider = "hardCodedBrowsers")
	public void getAmazonWebsite(String browser, String version, String os, Method method)
			throws UnexpectedException, MalformedURLException, InterruptedException {

		this.createDriver(browser, version, os, method.getName());
		WebDriver driver = this.getWebDriver();
		try {
			driver.get("https://amazon.com");
			driver.manage().window().maximize();

			// String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
			driver.findElement(By.linkText("Today's Deals")).click();
			Thread.sleep(2000);

			driver.findElement(By.linkText("Today's Deals")).click();
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block

		}
	}
}
