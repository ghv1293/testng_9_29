package com.testNg_sample;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MultiBrowser {
	
	public WebDriver driver;
	public static final String USERNAME = "ghv1293";
	  public static final String ACCESS_KEY = "c503f78a-f6db-4d77-82a3-b57e57ee306c";
	  public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	 
	  @Parameters("browser")
	 
	  @BeforeClass
	 
	  // Passing Browser parameter from TestNG xml
	 
	  public void beforeTest(String browser) throws MalformedURLException {
	 
	  // If the browser is Firefox, then do this
	 
	  if(browser.equalsIgnoreCase("firefox")) {

			 
		 /* System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\geckodriver.exe");
		  driver = new FirefoxDriver();*/
		  
		    DesiredCapabilities caps = DesiredCapabilities.firefox();
		    caps.setCapability("platform", "Windows 10");
		    caps.setCapability("version", "56.0");
		 
		    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);

	 
	  // If browser is IE, then do this	  
	 
	  }else if (browser.equalsIgnoreCase("ie")) { 
	 
		  // Here I am setting up the path for my IEDriver
	 
			/*String service = "C:\\Users\\hgundubo\\Desktop\\IEDriverServer.exe";
			System.setProperty("webdriver.ie.driver", service);
			InternetExplorerDriver  driver = new InternetExplorerDriver();*/
		  DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
		    caps.setCapability("platform", "Windows 10");
		    caps.setCapability("version", "11.1");
		 
		    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
			try {
				Thread.sleep(7000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 
	  } 
	  
	  else if (browser.equalsIgnoreCase("chrome")) { 
			 
		  // Here I am setting up the path for my IEDriver
	 
		  /*System.setProperty("webdriver.chrome.driver",  System.getProperty("user.dir")+"\\chromedriver.exe");
	 
		  driver = new ChromeDriver();*/
		    DesiredCapabilities caps = DesiredCapabilities.chrome();
		    caps.setCapability("platform", "Windows 10");
		    caps.setCapability("version", "60.0");
		 
		    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
	 
	  } 
	 
	 
	  driver.get("https://slickdeals.net/"); 
	 
	  }
	 
	  // Once Before method is completed, Test method will start
	 
	  @Test public void login() throws InterruptedException {
		Thread.sleep(2000);
	    WebElement element = driver.findElement(By.linkText("VIDEO GAMES"));
	    element.click();
	    Thread.sleep(2000);
		}  
	 
	  @AfterClass public void afterTest() {
	 
			driver.quit();
	 
		}

}
