package com.testNg_sample;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class ParallelExample {
	@Test
	public void setUp() {
		Reporter.log("Test1 is executed via Thread and Thread Id is " + Thread.currentThread().getId(), true);
	}

	@Test
	public void loginApplication() {
		Reporter.log("Test2 is executed via Thread and Thread Id is " + Thread.currentThread().getId(), true);
	}

	@Test
	public void logoutApplication() {
		Reporter.log("Test3 is executed via Thread and Thread Id is " + Thread.currentThread().getId(), true);
	}
}
